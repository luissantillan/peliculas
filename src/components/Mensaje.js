import React, { useState, Fragment } from 'react'
import { Modal, Button , Image, Col } from 'react-bootstrap'


function Mensaje(descripcion) {

    //const [titulo , detalle , imagen ] = descripcion ; 
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    return (

        <Fragment>
            <Button variant="primary" onClick={handleShow}>
                Ver Más
             </Button>
            <Modal show={show} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>{descripcion.titulo}</Modal.Title>
                </Modal.Header>
                <Modal.Body> 
                    <Col xs={6} md={4}>
                    <Image src={"https://image.tmdb.org/t/p/w500/" + descripcion.imagen} thumbnail="true" />
                </Col>
                {descripcion.detalle}
                </Modal.Body>
                <Modal.Footer>
                    <Button variant="secondary" onClick={handleClose}>
                        Cerrar
                     </Button>
                </Modal.Footer>
            </Modal>
        </Fragment>
    );
}

export default Mensaje; 