import React, { Fragment } from 'react';
import { Navbar, Nav  } from 'react-bootstrap'
import Noticias from './Noticias'


function Main() {

    return (
        <Fragment>
            <Navbar bg="light" expand="lg">
                <Navbar.Brand href="#home">Peliculas</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                    <Nav className="mr-auto">
                        {/*<Nav.Link href="#home">Home</Nav.Link>
                        <Nav.Link href="#link">Link</Nav.Link>*/ }
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
            <Noticias />
        </Fragment>
    );



}

export default Main; 