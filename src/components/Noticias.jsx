import React, { Fragment, useState, useEffect } from "react";
import { Form, Container, Row, Col, Card, Button } from "react-bootstrap";
import Mensaje from "./Mensaje";

const Noticias = () => {
  const [results, setPeliculas] = useState([]);
  const [genres, setGeneros]    = useState([]);
  const [count, setCount]       = useState(1);
  const [total, setTotal]       = useState(1);

  const [genero, setGenero]     = useState("28");

  const consultagenero = async () => {
    const solicitud =
      (await fetch(
        `https://api.themoviedb.org/3/genre/movie/list?api_key=98d39ab5462341809f5c82a0ca79fc54&language=es-ES`
      )) || [];
    const respuesta = await solicitud.json();
    setGeneros(respuesta.genres);
  };

  const consultaPel = async () => {
    const solicitud =
      (await fetch(
        `https://api.themoviedb.org/3/discover/movie?api_key=98d39ab5462341809f5c82a0ca79fc54&with_genres=${genero}&language=es-ES&page=${count}`
      )) || [];
    const respuesta = await solicitud.json();
    setTotal(respuesta.total_pages);
    setPeliculas( PrevS => [...results, ...respuesta.results]);
  };

  const consultarPeliculas = async () => {

    const solicitud =
      (await fetch(
        `https://api.themoviedb.org/3/discover/movie?api_key=98d39ab5462341809f5c82a0ca79fc54&with_genres=${genero}&language=es-ES&page=1`
      )) || [];
    const respuesta = await solicitud.json();
    setTotal(respuesta.total_pages); 
    setPeliculas(respuesta.results);

  };

  const consultCambiagen = (GeneroPelicula) => {
    setGenero(GeneroPelicula); 
  }

  useEffect(() => {
    consultarPeliculas();
  }, [genero]);

  useEffect(() => {
    consultaPel();
  }, [count] );

  useEffect(() => {
    consultagenero();
  }, [] );

  return (
    <Fragment>
      <Form.Group controlId="peliculas">
        <Container>
          <Row>
            <Col>
              <Form.Label>Genero</Form.Label>
              <Form.Control as="select" onChange={e => consultCambiagen(e.target.value)} >
               
                {genres.map(gen => {
                  return (
                    <option key={gen.id} value={gen.id} genero={gen.id}>
                      {gen.name}
                    </option>
                  );
                })}
              </Form.Control>
            </Col>
          </Row>
        </Container>
      </Form.Group>
      <Container>
        <Row className="row-cols-1 row-cols-sm-2 row-cols-md-4">
          {results.map(peliculas => (
            <Col className="d-flex">
              <Card border="success" style={{ height: "48rem" }}>
                <Card.Img
                  className="card-img-top"
                  variant="top"
                  src={
                    "https://image.tmdb.org/t/p/w500/" + peliculas.poster_path
                  }
                />
                <Card.Body>
                  <Card.Title>{peliculas.original_title}</Card.Title>
                  <Card.Text>
                    {peliculas.overview.substring(
                      0,
                      peliculas.overview.indexOf(".")
                    )}
                  </Card.Text>
                </Card.Body>
                <Card.Footer>
                  <Mensaje
                    titulo={peliculas.original_title}
                    detalle={peliculas.overview}
                    imagen={peliculas.poster_path}
                  />
                </Card.Footer>
              </Card>
            </Col>
          ))}
        </Row>
        {total > count && (
          <Button
            variant="dark"
            onClick={() => {
              setCount(count + 1);
            }}
          >
            {" "}
            Ver Más{" "}
          </Button>
        )}
      </Container>
    </Fragment>
  );
};

export default Noticias;
